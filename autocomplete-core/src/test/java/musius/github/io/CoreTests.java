package musius.github.io;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CoreTests {
  List<String> dict1;
  List<String> dict2;
  List<String> dict3;

  @Before
  public void setUp() throws Exception {
    dict1 = new ArrayList<>();
    dict1.add("a 1");
    dict1.add("aa 2");
    dict1.add("aaa 3");
    dict1.add("aaaa 4");
    dict1.add("aaaaa 5");

    dict2 = new ArrayList<>();
    dict2.add("kare 10");
    dict2.add("kanojo 20");
    dict2.add("karetachi 1");
    dict2.add("korosu 7");
    dict2.add("sakura 3");

    dict3 = new ArrayList<>();
    dict3.add("abcde 3");
    dict3.add("abcd 3");
    dict3.add("abc 3");
    dict3.add("a 3"); // intentional shuffle
    dict3.add("ab 3");
  }

  @After
  public void tearDown() throws Exception {}

  @Test
  public void testEmpty() throws IOException {
    Autocomplete autocomplete = new AutocompleteImpl();
    autocomplete.setUp(dict1);
    for (String prefix : new String[] {"b", "bc", "bb", "1d", "%d", "^", "z", "/", "aaaaaa",
        "aaaaab"}) {
      List<String> actual = autocomplete.getAutocompletionOn(prefix, 3);
      assertTrue(actual.isEmpty());
    }
  }

  @Test
  public void testOutputThreashold() throws IOException {
    Autocomplete autocomplete = new AutocompleteImpl();
    autocomplete.setUp(dict1);

    for (int i = 0; i < dict1.size(); i++) {
      List<String> actual = autocomplete.getAutocompletionOn("a", i);
      assertEquals(i, actual.size());
    }
    autocomplete.getAutocompletionOn("a", 100);
  }

  @Test
  public void testSimple() throws IOException {
    Autocomplete autocomplete = new AutocompleteImpl();
    autocomplete.setUp(dict2);

    List<String> actual = autocomplete.getAutocompletionOn("k");
    assertThat(actual, is(Arrays.asList("kanojo", "kare", "korosu", "karetachi")));
    actual = autocomplete.getAutocompletionOn("ka");
    assertThat(actual, is(Arrays.asList("kanojo", "kare", "karetachi")));
    actual = autocomplete.getAutocompletionOn("kar");
    assertThat(actual, is(Arrays.asList("kare", "karetachi")));
  }

  @Test
  public void testAlphabeticalOrderOfSameFrequencies() throws IOException {
    Autocomplete autocomplete = new AutocompleteImpl();
    autocomplete.setUp(dict3);

    List<String> actual = autocomplete.getAutocompletionOn("a");
    assertThat(actual, is(Arrays.asList("a", "ab", "abc", "abcd", "abcde")));
    actual = autocomplete.getAutocompletionOn("abc");
    assertThat(actual, is(Arrays.asList("abc", "abcd", "abcde")));
    actual = autocomplete.getAutocompletionOn("abcde");
    assertThat(actual, is(Arrays.asList("abcde")));
  }



}
