package musius.github.io;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

public interface Autocomplete {

  /**
   * Построить индекс автокомплита по словарю
   * 
   * @param dict в формате:
   *        <ul>
   *        <li>word1 frequency1</li>
   *        <li>word2 frequency2</li>
   *        </ul>
   * @throws IOException
   */
  void setUp(Iterable<String> dict) throws IOException;

  /**
   * Построить индекс автокомплита по словарю из потока
   * 
   * @param dict
   * @throws IOException
   */
  void setUp(Reader dict) throws IOException;

  List<String> getAutocompletionOn(String prefix, int maxNum) throws IOException;

  /**
   * Тоже самое что и getAutocompletionOn(String prefix, in maxNum), но maxNum = 10
   * 
   * @param prefix
   * @return
   * @throws IOException
   */
  List<String> getAutocompletionOn(String prefix) throws IOException;

}
