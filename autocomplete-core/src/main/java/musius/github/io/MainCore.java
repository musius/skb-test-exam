package musius.github.io;


import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class MainCore {
  public static void main(String[] args) throws IOException {
    Scanner in = new Scanner(new InputStreamReader(System.in, Charset.forName("ASCII")));

    Autocomplete autocomplete = new AutocompleteImpl();
    autocomplete.setUp(getDictFrom(in));
    int maxIterations = Integer.valueOf(in.nextLine()); // =M
    printAutocomplete(autocomplete, in, maxIterations);
  }

  private static List<String> getDictFrom(Scanner in) {
    List<String> firstPart = new ArrayList<>();
    int N = Integer.valueOf(in.nextLine());
    for (int i = 0; i < N; i++) {
      firstPart.add(in.nextLine());
    }
    return firstPart;
  }

  private static void printAutocomplete(Autocomplete a, Iterator<String> prefixes,
      int maxIterations) throws IOException {
    int i = 0;
    while (prefixes.hasNext() && i < maxIterations) {
      String prefix = prefixes.next();
      List<String> completions = a.getAutocompletionOn(prefix, 10);

      for (CharSequence completion : completions) {
        System.out.println(completion);
      }
      if (prefixes.hasNext()) {
        System.out.println();
      }
    }

  }
}
