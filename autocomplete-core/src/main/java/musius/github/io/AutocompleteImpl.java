package musius.github.io;

import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.lucene.search.suggest.FileDictionary;
import org.apache.lucene.search.suggest.InputIterator;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.tst.TSTLookup;
import org.apache.lucene.util.BytesRef;

public class AutocompleteImpl implements Autocomplete {

  private static class DictionaryIterator implements InputIterator {

    private Iterator<String> iterator;
    private String[] current;

    public DictionaryIterator(Iterator<String> iterator) {
      this.iterator = iterator;
    }

    @Override
    public BytesRef next() throws IOException {
      if (iterator.hasNext()) {
        current = iterator.next().split(" ");
        try {
          return new BytesRef(current[0].getBytes("ASCII"));
        } catch (UnsupportedEncodingException e) {
          throw new Error("Couldn't convert to ASCII");
        }
      } else {
        return null;
      }
    }

    @Override
    public long weight() {
      return Long.valueOf(current[1]);
    }

    @Override
    public BytesRef payload() {
      return null;
    }

    @Override
    public boolean hasPayloads() {
      return false;
    }

    @Override
    public Set<BytesRef> contexts() {
      return null;
    }

    @Override
    public boolean hasContexts() {
      return false;
    }
  }

  Lookup suggester;

  @Override
  public void setUp(Iterable<String> dict) throws IOException {
    suggester = new TSTLookup();
    suggester.build(new DictionaryIterator(dict.iterator()));
  }

  private Iterable<Lookup.LookupResult> lookupPrefix(String prefix, int maxNum) throws IOException {
    List<Lookup.LookupResult> lookup = suggester.lookup(prefix, true, maxNum);
    postLookupSorting(lookup);
    return lookup;
  }

  private void postLookupSorting(List<Lookup.LookupResult> lookup) {
    Collections.sort(lookup, new Comparator<Lookup.LookupResult>() {
      @Override
      public int compare(Lookup.LookupResult o1, Lookup.LookupResult o2) {
        int res = Long.compare(o2.value, o1.value);
        if (res == 0) {
          return o1.key.toString().compareTo(o2.key.toString());
        }
        return res;
      }
    });
  }

  @Override
  public List<String> getAutocompletionOn(String prefix, int maxNum) throws IOException {
    if (maxNum < 0) {
      throw new IllegalArgumentException("maxNum cannot be negative");
    }
    Iterable<Lookup.LookupResult> completions = lookupPrefix(prefix, maxNum);
    List<String> projection = new ArrayList<>();
    for (Lookup.LookupResult c : completions) {
      projection.add(c.key.toString());
    }
    return projection;
  }

  @Override
  public List<String> getAutocompletionOn(String prefix) throws IOException {
    return getAutocompletionOn(prefix, 10);
  }

  @Override
  public void setUp(Reader dictReader) throws IOException {
    suggester = new TSTLookup();
    suggester.build(new FileDictionary(dictReader, " "));
  }
}
