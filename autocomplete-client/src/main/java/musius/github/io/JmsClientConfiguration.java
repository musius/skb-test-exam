package musius.github.io;

import java.util.UUID;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan
@EnableTransactionManagement
public class JmsClientConfiguration {
  public static final String DEFAULT_DESTINATION_NAME = "customers";

  @Bean
  @Scope("prototype")
  public ConnectionFactory clientConnectionFactory(Environment env) throws Exception {
    ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
    String url = "tcp://" + env.getProperty(MainClient.ARGS_ADDRESS_PROPERTY_NAME, String.class);
    activeMQConnectionFactory.setBrokerURL(url);
    return activeMQConnectionFactory;
  }

  @Bean
  @Scope("prototype")
  public JmsTemplate clientJmsTemplate(ConnectionFactory clientConnectionFactory) throws Exception {
    JmsTemplate jmsTemplate = new JmsTemplate(clientConnectionFactory);
    jmsTemplate.setDefaultDestination(clientSendToQueue());
    jmsTemplate.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    return jmsTemplate;
  }

  @Bean
  @Scope("prototype")
  Queue clientReplyToQueue() {
    return new ActiveMQQueue(UUID.randomUUID().toString());
  }

  @Bean
  @Scope("prototype")
  Queue clientSendToQueue() {
    return new ActiveMQQueue("prefixes." + UUID.randomUUID().toString());
  }
}
