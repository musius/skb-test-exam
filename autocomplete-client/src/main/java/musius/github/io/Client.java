package musius.github.io;

import java.io.InputStream;

import javax.jms.JMSException;

public interface Client {

  /**
   * Все считать из стрима и отправить префиксы серверу, распечатать автокомплит
   * 
   * @param is
   * @throws JMSException
   */
  void start(InputStream is) throws JMSException;

}
