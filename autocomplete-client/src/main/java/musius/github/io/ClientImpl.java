package musius.github.io;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.SessionCallback;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ClientImpl implements Client {
  @Autowired
  JmsTemplate clientJmsTemplate;
  @Autowired
  Queue clientReplyToQueue;

  int msgcounter;

  @Override
  public void start(final InputStream is) throws JMSException {
    msgcounter = 0;
    doSend(is);
    doReceive();
  }

  private void doReceive() {
    clientJmsTemplate.execute(new SessionCallback<List<String>>() {
      @Override
      public List<String> doInJms(Session session) throws JMSException {
        MessageConsumer consumer = session.createConsumer(clientReplyToQueue);

        for (int i = 0; i < msgcounter; i++) {
          TextMessage m = (TextMessage) consumer.receive();
          System.out.println(m.getText());
          System.out.println();
        }
        return null;
      }
    }, true);
  }

  private void doSend(final InputStream is) {
    try (final Scanner scanner = new Scanner(is)) {
      while (scanner.hasNextLine()) {
        String payload = createBatchPayloadText(scanner);
        doSendInternal(payload);
      }
    }
  }

  private String createBatchPayloadText(final Scanner scanner) {
    msgcounter++;
    StringBuffer sb = new StringBuffer("get ");
    sb.append(scanner.nextLine());
    for (int i = 0; i < 100 && scanner.hasNextLine(); i++) {
      sb.append('\n');
      sb.append(scanner.nextLine());
    }
    return sb.toString();
  }

  private void doSendInternal(final String batchPayload) {
    clientJmsTemplate.send(new MessageCreator() {
      @Override
      public Message createMessage(Session session) throws JMSException {
        Message requestMessage = session.createTextMessage(batchPayload);
        // tricky part:
        // prevent jms from breaking the message order: JMSXGroupID
        requestMessage.setStringProperty("JMSXGroupID", "do_not_shuffle_please");
        requestMessage.setJMSReplyTo(clientReplyToQueue);
        return requestMessage;
      }
    });
  }
}
