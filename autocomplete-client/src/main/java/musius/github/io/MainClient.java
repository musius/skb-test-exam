package musius.github.io;

import java.io.BufferedInputStream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.CommandLinePropertySource;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.util.Assert;

public class MainClient {
  public static final String ARGS_ADDRESS_PROPERTY_NAME = "args.address";

  public static void main(String[] args) throws Exception {
    final ApplicationContext ctx = bootstrapContext(args);

    Client client = ctx.getBean(Client.class);
    try (BufferedInputStream in = new BufferedInputStream(System.in)) {
      client.start(in);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static AnnotationConfigApplicationContext bootstrapContext(String[] args) {
    final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
    ctx.getEnvironment().getPropertySources().addFirst(createCommandLinePropertySource(args));
    ctx.register(JmsClientConfiguration.class);
    ctx.registerShutdownHook();
    ctx.refresh();
    return ctx;
  }

  private static CommandLinePropertySource<?> createCommandLinePropertySource(String[] args) {
    Assert.isTrue(args.length == 1, "usage: program_execute <hostname>:<port>");
    CommandLinePropertySource<?> clps = new SimpleCommandLinePropertySource(args);
    clps.setNonOptionArgsPropertyName(ARGS_ADDRESS_PROPERTY_NAME);
    return clps;
  }
}
