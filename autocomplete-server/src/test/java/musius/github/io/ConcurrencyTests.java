package musius.github.io;

import java.io.File;
import java.io.FileInputStream;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSException;

import org.apache.activemq.broker.BrokerService;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.mock.env.MockPropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JmsClientConfiguration.class, JmsServerConfiguration.class},
    initializers = ConcurrencyTests.PropertyMocking.class)
public class ConcurrencyTests {

  @Resource(name = "embeddedBroker")
  BrokerService broker;
  @Inject
  ApplicationContext ctx;
  Client[] clients;

  @Before
  public void setUp() throws Exception {
    broker.start();
    clients = new Client[] {ctx.getBean(Client.class), ctx.getBean(Client.class),
        ctx.getBean(Client.class)};
  }

  @After
  public void tearDown() throws Exception {
    FileSystemUtils.deleteRecursively(new File("activemq-data"));
  }

  @Test
  public void testSimpleConcurrency() throws JMSException, InterruptedException {
    Thread a = thread(clients[0]);
    Thread b = thread(clients[1]);
    b.join();
    a.join();
  }

  public static Thread thread(final Client client) {
    Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        final File dic = FileUtils.getFile("src", "test", "resources", "prefixes.in");

        try (FileInputStream lines = new FileInputStream(dic)) {
          client.start(lines);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
    thread.setDaemon(false);
    thread.start();
    return thread;
  }

  public static class PropertyMocking
      implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
      MutablePropertySources propertySources =
          applicationContext.getEnvironment().getPropertySources();
      MockPropertySource mockEnvVars = new MockPropertySource()
          .withProperty(MainClient.ARGS_ADDRESS_PROPERTY_NAME, "localhost:61616")
          .withProperty(MainServer.ARGS_DICT_PORT_PROPERTY_NAME,
              new String[] {"src/test/resources/test.in", "61616"});
      propertySources.replace(StandardEnvironment.SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME,
          mockEnvVars);
    }
  }
}
