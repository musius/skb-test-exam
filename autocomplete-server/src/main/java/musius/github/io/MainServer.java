package musius.github.io;

import org.apache.activemq.broker.BrokerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.CommandLinePropertySource;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.util.Assert;

public class MainServer {
  public static final String ARGS_DICT_PORT_PROPERTY_NAME = "server_args_dict_port_name";

  public static void main(String[] args) throws Exception {
    final ApplicationContext ctx = bootstrapContext(args);

    final BrokerService broker = ctx.getBean(BrokerService.class);
    broker.start();
  }

  private static AnnotationConfigApplicationContext bootstrapContext(String[] args) {
    checkPreconditions(args);
    final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
    ctx.getEnvironment().getPropertySources().addFirst(createCommandLinePropertySource(args));
    ctx.register(JmsClientConfiguration.class);
    ctx.registerShutdownHook();
    ctx.refresh();
    return ctx;
  }

  private static void checkPreconditions(String[] args) {
    Assert.notEmpty(args);
    Assert.isTrue(args.length == 2, "Usage: program_execute <dictionary file> <port>");
  }

  private static CommandLinePropertySource<?> createCommandLinePropertySource(String[] args) {
    SimpleCommandLinePropertySource clps = new SimpleCommandLinePropertySource(args);
    clps.setNonOptionArgsPropertyName(ARGS_DICT_PORT_PROPERTY_NAME);
    return clps;
  }
}
