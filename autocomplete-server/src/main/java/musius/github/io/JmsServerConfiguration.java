package musius.github.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan
@EnableJms
public class JmsServerConfiguration {
  @Inject
  Environment env;

  @Bean
  Autocomplete serverAutocomplete() throws FileNotFoundException, IOException {
    AutocompleteImpl a = new AutocompleteImpl();
    final File dic = FileUtils.getFile(getDictArg());
    BufferedReader lines = Files.newBufferedReader(dic.toPath(), Charset.forName("ASCII"));
    a.setUp(lines);
    return a;
  }

  private String[] getEnvironmentArgs() {
    String[] property = env.getProperty(MainServer.ARGS_DICT_PORT_PROPERTY_NAME, String[].class);
    return property;
  }

  private String getDictArg() {
    String[] environmentArgs = getEnvironmentArgs();
    return environmentArgs[0];
  }

  private String getPortArg() {
    return getEnvironmentArgs()[1];
  }

  @Bean(name = "embeddedBroker")
  BrokerService broker() throws Exception {
    BrokerService broker = new BrokerService();
    broker.setUseShutdownHook(true);
    broker.addConnector("nio://0.0.0.0:" + getPortArg());
    broker.setPersistent(false);
    return broker;
  }

  @Bean
  public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(
      JmsTransactionManager serverJmsTransactionManager, Executor taskExecutor,
      ConnectionFactory serverConnectionFactory) throws Exception {
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    factory.setConnectionFactory(serverConnectionFactory());
    factory.setTransactionManager(serverJmsTransactionManager);
    factory.setConcurrency("10-60");
    factory.setTaskExecutor(taskExecutor);
    return factory;
  }

  @Bean
  public Executor taskExecutor() {
    return new ThreadPoolExecutor(10, 60, 10 * 60, TimeUnit.SECONDS,
        new ArrayBlockingQueue<Runnable>(20));
  }

  @Bean
  public ConnectionFactory serverConnectionFactory() throws Exception {
    ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
    factory.setBrokerURL("vm://localhost");
    return new CachingConnectionFactory(factory);
  }

  @Bean
  public JmsTransactionManager serverJmsTransactionManager(
      ConnectionFactory serverConnectionFactory) throws Exception {
    return new JmsTransactionManager(serverConnectionFactory);
  }
}
