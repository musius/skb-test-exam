package musius.github.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.jms.Session;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class Service {
  private static final String GET_PREFIX = "get ";
  @Autowired
  Autocomplete serverAutocomplete;

  @JmsListener(destination = "prefixes.*", concurrency = "10-60")
  public String autocompletePrefixes(String message, Session session) throws IOException {
    checkPreconditions(message);
    List<String> allCompletions = findAllAutocompletionsIn(message);
    return StringUtils.join(allCompletions, "\n\n");
  }

  private List<String> findAllAutocompletionsIn(String message) throws IOException {
    List<String> allCompletions = new ArrayList<>();
    try (Scanner sc = new Scanner(cutGetPrefix(message))) {
      while (sc.hasNextLine()) {
        String nextLine = sc.nextLine();
        List<String> completions = serverAutocomplete.getAutocompletionOn(nextLine);
        allCompletions.add(StringUtils.join(completions, '\n'));
      }
    }
    return allCompletions;
  }

  private void checkPreconditions(String message) {
    Assert.isTrue(message.startsWith(GET_PREFIX),
        "Unsupported message format. Message must be like: \"get <prefix>\"");
  }

  private String cutGetPrefix(String message) {
    return message.substring(GET_PREFIX.length());
  }
}
